(asdf:defsystem "claw-cplex-bindings"
  :description "Thin wrapper over IBM ILOG CPLEX, without functionality to load CPLEX"
  :version (:read-file-form "version.lisp-expr")
  :author "Model-based Embedded and Robotic Systems Group, MIT"
  :license "BSD-2"
  :pathname "bindings/"
  :depends-on ("uiop"
               "claw-cplex-bindings-1210"
               "claw-cplex-bindings-221")
  :components ((:file "packages")))

(asdf:defsystem "claw-cplex-bindings/common"
  :description "Common helpers for bindings"
  :version (:read-file-form "version.lisp-expr")
  :author "Model-based Embedded and Robotic Systems Group, MIT"
  :license "BSD-2"
  :pathname "bindings/"
  :depends-on ("alexandria" "cffi" "uiop")
  :components ((:file "common")))
