(asdf:defsystem "claw-cplex-config"
  :description "Configuration for claw-cplex system"
  :author "Model-based Embedded and Robotic Systems Group, MIT"
  :license "BSD-2"
  :version (:read-file-form "version.lisp-expr")
  :pathname "src/"
  :components ((:file "config")))
