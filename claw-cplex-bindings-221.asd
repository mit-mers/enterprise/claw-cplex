(asdf:defsystem "claw-cplex-bindings-221"
  :description "Wrapper over CPLEX v22.1, with some added enums that could not be auto-extracted"
  :version (:read-file-form "version.lisp-expr")
  :author "Model-based Embedded and Robotic Systems Group, MIT"
  :license "BSD-2"
  :pathname "bindings/221/"
  :depends-on ("claw-cplex-bindings-221-raw" "claw-cplex-bindings/common")
  :components ((:file "package")
               (:file "enum" :depends-on ("package"))))
