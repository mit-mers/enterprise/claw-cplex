(asdf:defsystem "claw-cplex-bindings-1210"
  :description "Wrapper over CPLEX v12.10, with some added enums that could not be auto-extracted"
  :version (:read-file-form "version.lisp-expr")
  :author "Model-based Embedded and Robotic Systems Group, MIT"
  :license "BSD-2"
  :pathname "bindings/1210/"
  :depends-on ("claw-cplex-bindings-1210-raw" "claw-cplex-bindings/common")
  :components ((:file "package")
               (:file "enum" :depends-on ("package"))))
