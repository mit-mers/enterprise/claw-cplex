(in-package #:claw-cplex)

(defvar *version-specific-package* nil)

(define-condition unknown-cplex-version (error)
  ((version
    :initarg :version))
  (:report (lambda (condition stream)
             (format stream "Loaded CPLEX version ~A, but unable to determine the version specific binding package to use."
                     (slot-value condition 'version)))))

(defun use-version (new-version &optional condition)
  "Invoke the USE-VERSION restart associated with CONDITION."
  (let ((restart (find-restart 'use-version condition)))
    (when restart
      (invoke-restart restart new-version))))

(defparameter *versions*
  '((("22" "1") #:claw-cplex-bindings-221 ("/opt/ibm/ILOG/CPLEX_Studio221/cplex/bin/x86-64_linux/"))
    (("12" "10") #:claw-cplex-bindings-1210 ("/opt/ibm/ILOG/CPLEX_Studio1210/cplex/bin/x86-64_linux/"))))

(defun unbind-all ()
  (do-external-symbols (sym :claw-cplex-bindings)
    (when (boundp sym)
      (makunbound sym))
    (when (fboundp sym)
      (let ((sym sym))
        (setf (fdefinition sym)
              (lambda (&rest args)
                (apply 'claw-cplex-bindings/unlinked::unbound-sentinel sym args)))))
    (when (ignore-errors (cffi::find-type-parser sym))
      (setf (cffi::find-type-parser sym) nil)))
  (setf *version-specific-package* nil))

(defun rebind (package)
  (do-external-symbols (sym package)
    (when (boundp sym)
      (setf (symbol-value (find-symbol (string sym) :claw-cplex-bindings))
            (symbol-value sym))
      (let ((sym sym))
        (setf (fdefinition (find-symbol (string sym) :claw-cplex-bindings))
              (lambda () (symbol-value sym)))))
    (when (fboundp sym)
      (setf (symbol-function (find-symbol (string sym) :claw-cplex-bindings))
            (symbol-function sym)))
    (when (ignore-errors (cffi::find-type-parser sym))
      (setf (cffi::find-type-parser (find-symbol (string sym) :claw-cplex-bindings))
            (cffi::find-type-parser sym))))
  (setf *version-specific-package* package))

(defun cplex-loaded-p ()
  "Return T if the CPLEX foreign library is loaded into the process."
  (not (null (cffi:foreign-symbol-pointer "CPXversion"))))

(defun cplex-loaded-and-linked-p ()
  "Return T if the CPLEX foreign library is loaded into the process and the
CLAW-CPLEX-BINDINGS package's symbols are bound to the correct version
specific values."
  (and (cplex-loaded-p)
       (not (null *version-specific-package*))))

;; We define this here in case we need to determine the version of an already
;; loaded CPLEX before we rebind packages.
(cffi:defcfun ("CPXopenCPLEX" %cpx-open-cplex) :pointer (status (:pointer :int)))
(cffi:defcfun ("CPXcloseCPLEX" %cpx-close-cplex) :int (env :pointer))
(cffi:defcfun ("CPXversion" %cpx-version) :string (env :pointer))

(defun cplex-version ()
  (cffi:with-foreign-object (status :int)
    (let ((env (%cpx-open-cplex status)))
      (unless (zerop (cffi:mem-ref status :int))
        (error "Unable to open CPLEX environment"))
      (unwind-protect
           (%cpx-version env)
        (cffi:with-foreign-object (envp :pointer)
          (setf (cffi:mem-ref envp :pointer) env)
          (%cpx-close-cplex envp))))))

(defun find-libcplex-in-dir (dir)
  (let ((files (directory (merge-pathnames "libcplex*.so" dir))))
    (first (sort files #'< :key (alex:compose #'length #'namestring)))))

(defun find-version-entry (version)
  (block nil
    (tagbody
     top
       (let* ((split (uiop:split-string version :separator '(#\.)))
              (entry (find split *versions* :key #'first :test (lambda (x y)
                                                                 (loop :for x1 :in x
                                                                       :for y1 :in y
                                                                       :always (equal x1 y1))))))
         (when (null entry)
           (restart-case (error 'unknown-cplex-version :version version)
             (use-version (new-version)
               :interactive (lambda ()
                              (format *query-io* "Enter version of CPLEX to use (unevaluated): ")
                              (list (read-line *query-io*)))
               :report "Pretend the loaded CPLEX is a different version"
               (setf version new-version)
               (go top))))
         (return entry)))))

(defun ensure-cplex-loaded (&key (library-path config:*libcplex-path*))
  "Ensure that CPLEX has been loaded and all symbols exported from the
CLAW-CPLEX-BINDINGS package have been bound to the values/functions/etc. from
the appropriate version specific package. Returns NIL (if the library could not
be loaded) or a string naming the version of CPLEX that is loaded.

May signal a UNKNOWN-CPLEX-VERSION condition if a version of CPLEX is loaded
which we do not have bindings for.

A specific library can be loaded using the LIBRARY-PATH argument.

If LIBRARY-PATH is NIL, the environment variable CPLEX_STUDIO_BINARIES is
checked. If it is set, it must point to a directory containing the CPLEX
binaries to load.

If LIBRARY-PATH is NIL and CPLEX_STUDIO_BINARIES is not set, this searchs
through some standard locations to try and find CPLEX, prefering newer versions
over older versions.

This function is automatically called when loaded if
CLAW-CPLEX-CONFIG:*LOAD-CPLEX-ON-LOAD-P* is non-NIL."
  ;; If CPLEX is not already loaded, try loading it.
  (flet ((%load-lib (pathname)
           ;; On SBCL, make sure the library isn't saved when dumping.
           #+sbcl
           (sb-alien:load-shared-object pathname :dont-save t)
           ;; On other Lisps, just use their defaults. Note that saved images
           ;; may not work quite right.
           #-sbcl
           (cffi:load-foreign-library pathname)))
    (unless (cplex-loaded-p)
      (cond
        ((not (null library-path))
         ;; User has asked for a specific library to be loaded.
         (%load-lib (pathname library-path)))
        ((uiop:getenvp "CPLEX_STUDIO_BINARIES")
         (let ((pathname (find-libcplex-in-dir (uiop:getenv-absolute-directory "CPLEX_STUDIO_BINARIES"))))
           (unless (null pathname)
             (%load-lib pathname))))
        (t
         (loop :for (nil nil paths) :in *versions*
               :for pathname := (some #'find-libcplex-in-dir paths)
               :unless (null pathname)
                 :do (%load-lib pathname)
                     (return)))))
    (when (cplex-loaded-p)
      (restart-case
          (let* ((version (cplex-version))
                 (entry (find-version-entry version)))
            (destructuring-bind (version package paths) entry
              (declare (ignore version paths))
              (unless (string-equal package *version-specific-package*)
                (unbind-all)
                (rebind package)))
            version)
        (abort ()
          :report "Do not bind CLAW-CPLEX-BINDINGS package.")))))

(defun dump-hook ()
  "This function should be called when dumping an image.

It is automatically added to UIOP's dump hooks if
CLAW-CPLEX-CONFIG:*ADD-TO-UIOP-DUMP-HOOK-P* is non-NIL."
  (unbind-all))

(when config:*add-to-uiop-dump-hook-p*
  (uiop:register-image-dump-hook 'dump-hook))

(defun restore-hook ()
  "This function should be called when loading an image.

It is automatically added to UIOP's restore hooks if
CLAW-CPLEX-CONFIG:*ADD-TO-UIOP-RESTORE-HOOK-P* is non-NIL."
  (ensure-cplex-loaded))

(when config:*add-to-uiop-restore-hook-p*
  (uiop:register-image-restore-hook 'restore-hook nil))

(when config:*load-cplex-on-load-p*
  (ensure-cplex-loaded))
