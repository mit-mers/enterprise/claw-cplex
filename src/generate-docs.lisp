(in-package #:claw-cplex)

(defmacro doc-ignore-cplex-bindings ()
  `(progn
     ,@(loop
         :for package :in '(#:claw-cplex-bindings #:claw-cplex-bindings-1210 #:claw-cplex-bindings-221)
         :collect
         `(eval-when (:compile-toplevel :load-toplevel :execute)
            (let ((*package* (find-package ',package)))
              (40ants-doc/ignored-words:ignore-words-in-package
               ,@(loop
                   :for sym :being :the :external-symbols :in (find-package package)
                   :collect (list 'quote sym))))))))

(doc-ignore-cplex-bindings)

(defun generate-docs ()
  (40ants-doc/builder:update-asdf-system-docs @index :claw-cplex))
