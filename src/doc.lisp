(in-package #:claw-cplex)

(progn
  (doc:defsection @index (:title "CLAW CPLEX"
                          :ignore-words #1=("ASDF" "BINARIES" "BSD-2" "CL" "CLAW" "CPLEX"
                                                   "IBM" "ILOG" "MIT" "STUDIO" "UIOP")
                          :export nil)
    #.(uiop:read-file-string (merge-pathnames "../README.md"
                                              (or *compile-file-pathname* *load-pathname*)))
    (@claw-cplex section)
    (@claw-cplex-config section)
    (@claw-cplex-bindings section))

  (doc:defsection @claw-cplex (:title "claw-cplex"
                               :ignore-words #1#
                               :export nil)
    "Typically, your system will depend on the CLAW-CPLEX system. Loading this
system will load all the bindings, as well as some helper functions to actually
load the CPLEX foreign library, determine its version, and rebind functions and
variables in the CLAW-CPLEX-BINDINGS package to the correct foreign
definitions."
    (claw-cplex system)
    (@claw-cplex/exports section))

  (doc:defsection @claw-cplex/exports (:title "claw-cplex exports"
                                       :ignore-words #1#
                                       :export nil)
    (claw-cplex package)
    (cplex-loaded-p function)
    (cplex-loaded-and-linked-p function)
    (ensure-cplex-loaded function)
    (dump-hook function)
    (restore-hook function)
    (unknown-cplex-version condition)
    (use-version restart))

  (doc:defsection @claw-cplex-config (:title "claw-cplex-config"
                                      :ignore-words #1#
                                      :export nil)
    "The CLAW-CPLEX-CONFIG system loads the CLAW-CPLEX-CONFIG package, and allows
you to configure the CLAW-CPLEX system before loading it."
    (claw-cplex-config system)
    (@claw-cplex-config/exports section))

  (doc:defsection @claw-cplex-config/exports (:title "claw-cplex-config exports"
                                              :ignore-words #1#
                                              :export nil)
    (claw-cplex-config package)
    (config:*add-to-uiop-dump-hook-p* variable)
    (config:*add-to-uiop-restore-hook-p* variable)
    (config:*libcplex-path* variable)
    (config:*load-cplex-on-load-p* variable))

  (doc:defsection @claw-cplex-bindings (:title "claw-cplex-bindings"
                                        :ignore-words #1#
                                        :export nil)
    "The CLAW-CPLEX-BINDINGS system loads the bindings for every supported version
of CPLEX."
    (claw-cplex-bindings system)
    (@claw-cplex-bindings/packages section)
    (@claw-cplex-bindings/exports section))

  (doc:defsection @claw-cplex-bindings/packages (:title "claw-cplex-bindings packages"
                                                 :ignore-words #1#
                                                 :export nil)
    "You should not need to use any symbols from these packages directly. But they
are available in case you need to support only a very specific version of CPLEX."
    . #.(loop :for package :in (list-all-packages)
              :for name := (package-name package)
              :when (and
                     (uiop:string-prefix-p (uiop:standard-case-symbol-name '#:claw-cplex-bindings-)
                                           name)
                     (not (uiop:string-suffix-p name (uiop:standard-case-symbol-name '#:pristine)))
                     (not (uiop:string-suffix-p name (uiop:standard-case-symbol-name '#:raw))))
                :collect (list (intern name) 'package)))

  (doc:defsection @claw-cplex-bindings/exports (:title "claw-cplex-bindings exports"
                                                :ignore-words #1#
                                                :export nil)
    "The CLAW-CPLEX-BINDINGS package exports all symbols corresponding to foreign
variables and functions. The values of those symbols are bound to the correct
foreign definitions by CLAW-CPLEX:ENSURE-CPLEX-LOADED."
    (claw-cplex-bindings package)
    (claw-cplex-bindings/unlinked package)
    (symbol-not-linked condition)
    (symbol-not-linked-name (reader symbol-not-linked))
    (link restart)
    (link function)))

(40ants-doc/restart:define-restart use-version (new-version)
  "Available when UNKNOWN-CPLEX-VERSION error is signalled. Use this restart to
pretend the loaded CPLEX version is NEW-VERSION, and use the bindings generated
for NEW-VERSION.")

(40ants-doc/restart:define-restart link (&key library-path &allow-other-keys)
  "May be available when SYMBOL-NOT-LINKED error is signalled. Attempt to link
CPLEX and re-call the function.

Is not available if CLAW-CPLEX package is not loaded or CPLEX has already
been linked.")
