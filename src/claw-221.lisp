(uiop:define-package #:%claw-cplex/wrapper-221
  (:use #:cl))

(claw:defwrapper (:claw-cplex
                  (:headers "ilcplex/cplexx.h")
                  (:includes "/opt/ibm/ILOG/CPLEX_Studio221/cplex/include/")
                  (:targets ((:and :x86-64 :linux) "x86_64-pc-linux-gnu")
                            ((:and :x86-64 :windows) "x86_64-pc-windows-msvc"))
                  (:persistent :claw-cplex-bindings-221-raw
                   :asd-path "../claw-cplex-bindings-221-raw.asd"
                   :bindings-path "../bindings/221/")
                  (:include-definitions "^(cpx|CPX)\\w+"))
  :inline-functions nil
  :in-package :%claw-cplex-bindings-221
  :trim-enum-prefix t
  :recognize-bitfields t
  :recognize-strings t
  :symbolicate-names (:in-pipeline
                      ;; the 64-bit API says to call CPXX functions. But those
                      ;; are actually static inline functions that call
                      ;; functions prefixed with CPXL or CPXS depending on what
                      ;; size model CPLEX decides the paltform can best
                      ;; support. Normally we could just strip those prefixes
                      ;; off. But unfortunately, CPLEX has some types that
                      ;; start with those prefixes, such as: CPXLPptr. We want
                      ;; CPXLPptr to turn into lp-ptr instead of p-ptr. It
                      ;; seems that the CPXL prefix we want to remove is never
                      ;; followed by another capital letter, except NET, which
                      ;; gives us the :by-replacing part of the pipeline.
                      (:by-replacing "CPXLNET" "CPXL_NET_")
                      (:by-replacing "CPXSNET" "CPXS_NET_")
                      (:by-replacing "^CPXL([a-z])" "CPXL_\\1")
                      (:by-replacing "^CPXS([a-z])" "CPXS_\\1")
                      ;; Otherwise names like CENVptr get turned into cen-vptr
                      (:by-changing-postfix
                       "ptr" "_ptr")
                      (:by-removing-prefixes
                       "CPXL_" "CPXS_" "cpxl_" "cpxs_"
                       "CPX_" "cpx_" "CPX" "cpx")))
