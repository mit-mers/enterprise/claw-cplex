(defpackage #:claw-cplex
  (:use #:cl)
  (:local-nicknames (#:alex #:alexandria)
                    (#:config #:claw-cplex-config)
                    (#:doc #:40ants-doc))
  (:import-from #:claw-cplex-bindings/unlinked
                #:link
                #:symbol-not-linked
                #:symbol-not-linked-name)
  (:export #:cplex-loaded-p
           #:cplex-loaded-and-linked-p
           #:dump-hook
           #:ensure-cplex-loaded
           #:link
           #:restore-hook
           #:symbol-not-linked
           #:symbol-not-linked-name
           #:unknown-cplex-version
           #:use-version))
