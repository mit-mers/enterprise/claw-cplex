(uiop:define-package #:claw-cplex-config
  (:use #:cl)
  (:export #:*add-to-uiop-dump-hook-p*
           #:*add-to-uiop-restore-hook-p*
           #:*libcplex-path*
           #:*load-cplex-on-load-p*))

(in-package #:claw-cplex-config)

(defparameter *add-to-uiop-dump-hook-p* t
  "If non-NIL when CLAW-CPLEX system is loaded, CLAW-CPLEX:DUMP-HOOK is added to
UIOP's list of dump hooks.")

(defparameter *add-to-uiop-restore-hook-p* t
  "If non-NIL when CLAW-CPLEX system is loaded, CLAW-CPLEX:RESTORE-HOOK is added to
UIOP's list of restore hooks.")

(defparameter *libcplex-path* nil
  "The default value for the :LIBRARY-PATH argument to
CLAW-CPLEX:ENSURE-CPLEX-LOADED function.")

(defparameter *load-cplex-on-load-p* t
  "If non-NIL when CLAW-CPLEX system is loaded, CLAW-CPLEX:ENSURE-CPLEX-LOADED is
automatically called.")
