(require "asdf")

;; No actual tests for now (until/if we figure out how to safely install CPLEX
;; in CI). Just make sure it loads.
(asdf:test-system :claw-cplex)
