;;;; Script to generate bindings for a specific CPLEX version.

(in-package #:cl-user)

(require :asdf)

(pushnew :claw-regen-adapter *features*)

(asdf:load-system "adopt")
(asdf:load-system "cffi")

(defparameter *option-libresect-path*
  (adopt:make-option :libresect-path
                     :long "libresect-path"
                     :initial-value "/usr/local/src/libresect/build/resect/libresect.so"
                     :help "The path to libresect.so"
                     :parameter "PATH"
                     :reduce #'adopt:last))

(defparameter *ui*
  (adopt:make-interface :name "generate-bindings"
                        :help "Generate bindings for a specific version of CPLEX"
                        :summary "Generate bindings for a specific version of CPLEX"
                        :usage "generate-bindings [OPTIONS]"
                        :contents (list *option-libresect-path*)))

(adopt:defparameters (*args* *options*) (adopt:parse-options-or-exit *ui*))

(when (null *args*)
  (format *error-output* "You must specify the version of CPLEX to build wrapper for.~%")
  (uiop:quit 1))

(let ((path (gethash :libresect-path *options*)))
  (format t ";; Loading ~A~%" path)
  (cffi:load-foreign-library path))

(asdf:load-system "claw")
(asdf:load-system "claw-utils")
(let ((version (first *args*)))
  (cond
    ((equal version "12.10")
     (asdf:load-system "claw-cplex/wrapper-1210"))
    ((equal version "22.1")
     (asdf:load-system "claw-cplex/wrapper-221"))))

(claw:load-wrapper :claw-cplex)

(uiop:quit)
