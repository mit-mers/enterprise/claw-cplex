(asdf:defsystem "claw-cplex"
  :description "Thin wrapper over IBM ILOG CPLEX"
  :version (:read-file-form "version.lisp-expr")
  :author "Model-based Embedded and Robotic Systems Group, MIT"
  :license "BSD-2"
  :pathname "src/"
  :depends-on ("claw-cplex-bindings" "claw-cplex-config" "40ants-doc")
  :components ((:file "package")
               (:file "bind" :depends-on ("package"))
               (:file "doc" :depends-on ("package"))))

(asdf:defsystem "claw-cplex/doc-generator"
  :description "Documentation generation for CLAW-CPLEX"
  :version (:read-file-form "version.lisp-expr")
  :author "Model-based Embedded and Robotic Systems Group, MIT"
  :license "BSD-2"
  :pathname "src/"
  :depends-on ("40ants-doc-full" "claw-cplex")
  :components ((:file "generate-docs")))

(asdf:defsystem "claw-cplex/wrapper-1210"
  :description "Wrapper generator for CPLEX v12.10"
  :version (:read-file-form "version.lisp-expr")
  :author "Model-based Embedded and Robotic Systems Group, MIT"
  :license "BSD-2"
  :depends-on ("uiop" "claw" "cffi")
  :pathname "src/"
  :serial t
  :components ((:file "claw-1210")))

(asdf:defsystem "claw-cplex/wrapper-221"
  :description "Wrapper generator for CPLEX v22.1"
  :version (:read-file-form "version.lisp-expr")
  :author "Model-based Embedded and Robotic Systems Group, MIT"
  :license "BSD-2"
  :depends-on ("uiop" "claw" "cffi")
  :pathname "src/"
  :serial t
  :components ((:file "claw-221")))
