# CLAW-CPLEX changelog #

## Unreleased

## v0.2.0 - May 15, 2022

+ No longer generate bindings for CPXXmsg.
+ All CPLEX parameters can now be accessed using a zero-argument function of
  the same name.
+ `CPLEX-LOADED-AND-BOUND-P` renamed to `CPLEX-LOADED-AND-LINKED-P`.
+ When CPLEX functions are unlinked, the corresponding CL functions are no
  longer left unbound. Instead, they are bound to a function that signals a
  `SYMBOL-NOT-LINKED` condition, with `USE-VALUE` and `LINK` restarts.
+ Added documentation.

## v0.1.1 - May 12, 2022

First release!

Re-released due to a CI mixup.

## v0.1.0 - May 12, 2022

First release!
