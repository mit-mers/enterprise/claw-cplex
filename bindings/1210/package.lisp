(uiop:define-package #:claw-cplex-bindings-1210
  (:use)
  (:use-reexport #:%claw-cplex-bindings-1210)
  (:export #:conflict-member-status
           #:solution-status)
  (:documentation
   "Bindings for CPLEX version 12.10"))
