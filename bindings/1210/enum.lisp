(in-package #:%claw-cplex-bindings-common)

(make-enum :%claw-cplex-bindings-1210 claw-cplex-bindings-1210:conflict-member-status
           (+conflict- +))
(make-enum :%claw-cplex-bindings-1210 claw-cplex-bindings-1210:solution-status
           (+stat- +)
           (+mip- + :mip-))
