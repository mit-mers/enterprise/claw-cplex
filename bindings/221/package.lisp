(uiop:define-package #:claw-cplex-bindings-221
  (:use)
  (:use-reexport #:%claw-cplex-bindings-221)
  (:export #:conflict-member-status
           #:solution-status)
  (:documentation
   "Bindings for CPLEX version 22.1"))
