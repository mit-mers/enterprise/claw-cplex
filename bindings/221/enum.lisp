(in-package #:%claw-cplex-bindings-common)

(make-enum :%claw-cplex-bindings-221 claw-cplex-bindings-221:conflict-member-status
           (+conflict- +))
(make-enum :%claw-cplex-bindings-221 claw-cplex-bindings-221:solution-status
           (+stat- +)
           (+mip- + :mip-))
