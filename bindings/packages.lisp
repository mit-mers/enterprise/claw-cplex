(uiop:define-package #:claw-cplex-bindings
  (:use)
  (:reexport #:claw-cplex-bindings-1210)
  (:reexport #:claw-cplex-bindings-221)
  (:documentation
   "Exports symbols with names equal to the union of all symbols exported from the
version specific packages. Initially, all the variables are unbound and all
functions immediately signal a CLAW-CPLEX-BINDINGS/UNLINKED:SYMBOL-NOT-LINKED
error. Use CLAW-CPLEX:ENSURE-CPLEX-LOADED to bind all variable and functions to
the correct definitions for the version of CPLEX that has been loaded."))

(uiop:define-package #:claw-cplex-bindings/unlinked
  (:use #:cl)
  (:export #:link
           #:symbol-not-linked
           #:symbol-not-linked-name)
  (:documentation
   "Exports symbols for signalling that a symbol is unlinked to a foreign
definition and remedying that."))

(cl:in-package #:claw-cplex-bindings/unlinked)

(do-external-symbols (v :claw-cplex-bindings)
  (when (and (uiop:string-enclosed-p "+" (string v) "+"))
    (proclaim `(special ,v))))

(define-condition symbol-not-linked (error)
  ((name
    :initarg :name
    :reader symbol-not-linked-name))
  (:report (lambda (c stream)
             (format stream "The symbol ~A is not linked to the CPLEX foreign library"
                     (symbol-not-linked-name c)))))

(defun link (&rest args &key condition library-path &allow-other-keys)
  "Invoke the LINK restart associated with CONDITION."
  (declare (ignore library-path))
  (let ((restart (find-restart 'link condition)))
    (when restart
      (apply #'invoke-restart restart (uiop:remove-plist-key :condition args)))))

(defun unbound-sentinel (name &rest args)
  (restart-case
      (error 'symbol-not-linked :name name)
    (link (&rest restart-args &key &allow-other-keys)
      :report "Attempt to link CPLEX and re-call the function"
      :interactive (lambda ()
                     (let ((args ()))
                       (format *query-io* "Enter library path or nothing for the default (unevaluated): ")
                       (finish-output *query-io*)
                       (let ((path (read-line *query-io*)))
                         (unless (equal "" path)
                           (push path args)
                           (push :library-path args)))
                       args))
      :test (lambda (c)
              (declare (ignore c))
              (and (find-package '#:claw-cplex)
                   (not (uiop:symbol-call '#:claw-cplex '#:cplex-loaded-and-linked-p))))
      (apply 'uiop:symbol-call '#:claw-cplex '#:ensure-cplex-loaded restart-args)
      (apply name args))
    (use-value (value)
      value)))

(do-external-symbols (sym :claw-cplex-bindings)
  (let ((sym sym))
    (unless (fboundp sym)
      (setf (fdefinition sym) (lambda (&rest args) (apply 'unbound-sentinel sym args))))))
