(defpackage #:%claw-cplex-bindings-common
  (:use #:cl)
  (:local-nicknames (#:alex #:alexandria))
  (:export #:make-enum))

(in-package #:%claw-cplex-bindings-common)

(defmacro make-enum (package enum-name &rest config)
  (let ((package (find-package package))
        (symbols ()))
    (loop :for (prefix suffix new-prefix) :in config
          :do
             (let ((prefix (string prefix))
                   (suffix (string suffix)))
               (do-external-symbols (sym package)
                 (let ((name (string sym)))
                   (when (uiop:string-prefix-p prefix (string sym))
                     (let ((remaining (subseq name (length prefix))))
                       (when (uiop:string-suffix-p remaining suffix)
                         (let ((remaining (subseq remaining 0 (- (length remaining) (length suffix)))))
                           (push (list (alex:make-keyword
                                        (format nil "~@[~A~]~A" new-prefix remaining))
                                       (symbol-value sym))
                                 symbols)))))))))
    `(cffi:defcenum ,enum-name ,@(sort symbols #'< :key #'second))))
